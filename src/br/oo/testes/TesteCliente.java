package br.oo.testes;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.oo.pojos.Cliente;
import br.oo.pojos.Conta;

public class TesteCliente {
	private Cliente cliente;
	private Conta conta;

	@Before
	public void setup() {
		this.conta = new Conta("7777-77", 'C', 20000.0);
		this.cliente = new Cliente();
		cliente.setNome("Phelipe");
		cliente.setTipo('F');
		cliente.setConta(conta);
	}

	@Test
	public void testeVerSaldo() {
		assertEquals(20000, cliente.verSaldo(), 0.01);
	}
	
	@Test
	public void testaDeposita(){
		cliente.depositar(10000);
		assertEquals(30000, cliente.getConta().getSaldo(), 0.01);
	}
	
	@Test
	public void testeSacar(){
		cliente.sacar(10000);
		assertEquals(10000, cliente.getConta().getSaldo(), 0.01);
	}
}
