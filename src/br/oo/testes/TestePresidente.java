package br.oo.testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.oo.controllers.Banco;
import br.oo.pojos.Diretor;
import br.oo.pojos.Funcionario;
import br.oo.pojos.Presidente;

public class TestePresidente {

	private Funcionario funcionario;
	private Presidente presidente;
	private Banco banco;

	@Before
	public void setup() {
		this.funcionario = new Funcionario();
		funcionario.setNome("Phelipe");
		funcionario.setTipo('F');
		this.banco = new Banco();
		this.presidente = new Presidente();
	}
	
	@Test
	public void testaContrataFuncionarios(){
		presidente.contrataFuncionario(funcionario, banco);
		assertEquals("Phelipe", this.banco.getPessoas().get(0).getNome());
	}
}
