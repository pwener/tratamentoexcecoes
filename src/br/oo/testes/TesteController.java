package br.oo.testes;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.oo.controllers.Banco;
import br.oo.controllers.ControleBancario;
import br.oo.pojos.Conta;

public class TesteController {
	private ControleBancario controle;

	@Before
	public void setup(){
		Banco b = new Banco();
		this.controle = new ControleBancario(b);
	}
	
	@Test
	public void adicionaContaTeste() {
		Conta c = new Conta();
		c.setNumero("1232");
		controle.adiciona(c);
		assertEquals("1232", controle.getBanco().getContas().get(0).getNumero());
	}
	
	@Test
	public void pegaConta(){
		Conta c = new Conta("10", 'F', 2000);
		controle.adiciona(c);
		assertEquals("10", controle.pegaConta("10").getNumero());
	}
	
	@Test
	public void pegaTotalTeste(){
		assertEquals(0, controle.pegaTotalDeContas());
	}

}
