package br.oo.controllers;

import br.oo.exceptions.ValorInvalidoException;
import br.oo.pojos.Cliente;

public class TestaDeposita2 {

	public static void main(String[] args) {
		Cliente c = new Cliente();
		try{
			c.depositar(-100);
		}catch(ValorInvalidoException e){
//			System.out.println("Ocorreu um erro: " + e.getMessage());
			System.out.println(e.getMessage());
		}
	}

}
