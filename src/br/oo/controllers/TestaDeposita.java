package br.oo.controllers;

import br.oo.pojos.Cliente;

public class TestaDeposita {
	// Irá retornar o IllegalArgumentException
	public static void main(String[] args) {
		Cliente c = new Cliente();
		try {
			c.depositar(-100);
		} catch (IllegalArgumentException i) {
			System.out.println("Ocorreu um erro do tipo: " + i.getMessage());
		}
	}
}
