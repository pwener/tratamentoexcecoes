package br.oo.pojos;

public class Funcionario extends Pessoa{
	protected String setor;
	//Cada funcionario tem um código
	protected int codigo;
	protected double salario;
	
	public String getSetor() {
		return setor;
	}
	public void setSetor(String setor) {
		this.setor = setor;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	
}
