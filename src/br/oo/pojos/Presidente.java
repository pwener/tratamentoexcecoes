package br.oo.pojos;

import br.oo.controllers.Banco;

public class Presidente extends Gerente {
	
	public Funcionario pesquisaFuncionario(String nome, Banco banco) {
		for (Pessoa p : banco.getPessoas()) {
			if (p.getNome() == nome) {
				return (Funcionario) p;
			}
		}
			return null;
	}

	public void contrataFuncionario(Pessoa pessoa, Banco banco) {
		banco.getPessoas().add(pessoa);
	}
}
