package br.oo.pojos;

import br.oo.exceptions.ValorInvalidoException;

public class Cliente extends Pessoa {
	private Conta conta;

	public  void sacar(double quantidade) {
		this.conta.setSaldo(getConta().getSaldo() - quantidade);
	}

	public void depositar(double quantidade) {
		if(quantidade < 0){
//			throw new IllegalArgumentException("Valor inválido!");
//			throw new ValorInvalidoException();
			throw new ValorInvalidoException(quantidade);
		}else{
			this.conta.setSaldo(getConta().getSaldo() + quantidade);
		}
	}

	public double verSaldo() {
		return conta.getSaldo();
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

}
