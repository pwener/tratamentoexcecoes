package br.oo.exceptions;

public class ValorInvalidoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ValorInvalidoException() {
		super("Valor inválido");
	}

	public ValorInvalidoException(double valor) {
		super("Valor inválido: " + valor + " por favor digite outro valor");
	}
}
